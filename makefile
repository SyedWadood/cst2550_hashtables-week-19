CXX = g++
CXXFLAGS = -Wall -Wextra -Wpedantic -std=c++17

.PHONY : all clean
all : frequency_counter

frequency_counter : frequency_counter.cpp hash.h
	$(CXX) $(CXXFLAGS) -o $@ $<

clean :
	$(RM) frequency_counter

